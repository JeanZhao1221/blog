---
title: Array Usage Collection
date: 2018-07-03 14:06:25
tags: Javascript, Array, ECMA 
---

I'm so used to using array prototype methods because it makes my code simpler and more efficient. I could focus more on logic design or business service. What so many kinds of methods! Sometimes, we may be confused about when and how to use, or when the source array are secretly forced to be changed...

<!-- more -->

So I made a short summary here just for reference:

## Go Through
*In order to do other actions when going through an array.*

<table class="col-3">
    <tr>
        <th>Methods</th>
        <th>Comments</th>
        <th>Updates in</th>
    </tr>
    <tr>
        <td><p class='code'>forEach</p></td>
        <td></td>
        <td>ES5</td>
    </tr>
    <tr>
        <td><p class='code'>reduce</p></td>
        <td></td>
        <td>ES5</td>
    </tr>
    <tr>
        <td><p class='code'>reduceRight</p></td>
        <td>the same as "reserve().reduce()"</td>
        <td>ES5</td>
    </tr>
</table>

## Find
*In order to find an item element in the array.*

<table class="col-3">
    <tr>
        <th>Methods</th>
        <th>Comments</th>
        <th>Updates in</th>
    </tr>
    <tr>
        <td><p class='code'>array.find(callback)</p></td>
        <td>return an item from an array</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.findIndex(callback)</p></td>
        <td>return index number</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.indexOf(element[, fromIndex])</p></td>
        <td>return index number</td>
        <td>ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.lastIndexOf(element[, fromIndex])</p></td>
        <td>return index number</td>
        <td>ES5</td>
    </tr>
</table>

## Check
*It is used for checking with a callback in an array.*

<table class="col-3">
    <tr>
        <th>Methods</th>
        <th>Comments</th>
        <th>Updates in</th>
    </tr>
    <tr>
        <td><p class='code'>array.every(callback)</p></td>
        <td>return true only when all elements in the array pass the callback</td>
        <td>ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.includes(element[, fromIndex])</p></td>
        <td>return true only when an array includes a certain element</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.some(callback)</p></td>
        <td>return true when at least one element in the array passes the callback</td>
        <td>ES5</td>
    </tr>
</table>

## Type Conversion ##
### To be other type  ###

<table class="col-3">
    <tr>
        <th>Methods</th>
        <th>Comments</th>
        <th>Updates in</th>
    </tr>
    <tr>
        <td><p class='code'>array.entries()</p></td>
        <td>returns a new <p class='code'>Array Iterator</p> object that contains the key/value pairs for each index in the array.</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.keys()</p></td>
        <td>returns a new <p class='code'>Array Iterator</p> object that contains the keys for each index in the array</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.values()</p></td>
        <td>returns a new <p class='code'>Array Iterator</p> object that contains the values for each index in the array.</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.join()</p></td>
        <td>joins all elements of an array (or an array-like object) into a string and returns this string.</td>
        <td>ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.toString()</p></td>
        <td>same as array.join(',')</td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.toLocalString()</p></td>
        <td>use toLocalString method of object, number, Date and then join all elements with ','</td>
        <td>before ES5</td>
    </tr>
</table>

### To Be an Array from Other Array-like Object or Iterable Object ###

<table class="col-3">
    <tr>
        <th>Methods</th>
        <th>Comments</th>
        <th>Updates in</th>
    </tr>
    <tr>
        <td><p class='code'>Array.from(array-like)</p></td>
        <td>creates a new, shallow-copied Array instance from an array-like or iterable object.</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>Array.of(element0[...,[elementN]])</p></td>
        <td>creates a new Array instance with a variable number of arguments, regardless of number or type of the arguments.</td>
        <td>ES6</td>
    </tr>
</table>

## Update Source Array ##
*The source array would be modified when invoking following methods.*

<table class="col-3">
    <tr>
        <th>Methods</th>
        <th>Comments</th>
        <th>Updates in</th>
    </tr>
    <tr>
        <td><p class='code'>array.splice(start[, deleteCount[, item1[, ...[,itemN]]]])</p></td>
        <td>remove existing elements and/or add new elements</td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.copyWithin(target[,start[,end]])</p></td>
        <td>e.g. var a=[1,2,3]; a.copyWithin(1,2,3); //expected a: [1,3, 3]</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.fill(value[,start[,end]])</p></td>
        <td>e.g. var a=[1,2,3]; a.fill(1,2,3); //expected a: [1,2,1]</td>
        <td>ES6</td>
    </tr>
    <tr>
        <td><p class='code'>array.pop()</p></td>
        <td></td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.push(element1[, ...[,elementN]])</p></td>
        <td>Array.prototype.push.apply(array1, array2) equals array1 = array1.concat(array2);</td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.sort(callback)</p></td>
        <td></td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.reverse()</p></td>
        <td>e.g. var a=["a", "c", "d"]; a.reverse(); //expected a: ["d", "c", "a"]</td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.shift()</p></td>
        <td>e.g. var a=["a", "c", "d"]; a.shift(); //expected a: ["c", "d"]</td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.unshift()</p></td>
        <td>e.g. var a=["a", "c", "d"]; a.unshift("e","f"); //expected a: ["e", "f", "a", "c", "d"]</td>
        <td>before ES5</td>
    </tr>
</table>

## Create A New Array and Not Affect Source Array ##

<table class="col-3">
    <tr>
        <th>Methods</th>
        <th>Comments</th>
        <th>Updates in</th>
    </tr>
    <tr>
        <td><p class='code'>array1.concat(array2)</p></td>
        <td></td>
        <td>before ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.filter(callback)</p></td>
        <td></td>
        <td>ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.map(callback)</p></td>
        <td></td>
        <td>ES5</td>
    </tr>
    <tr>
        <td><p class='code'>array.flatMap(callback)</p></td>
        <td></td>
        <td><p class='code'>experimental API</p></td>
    </tr>
    <tr>
        <td><p class='code'>array.flat(depth)</p></td>
        <td></td>
        <td><p class='code'>experimental API</p></td>
    </tr>
    <tr>
        <td><p class='code'>array.slice(begin[, end])</p></td>
        <td>e.g. var a=[1,2,3]; var b = a.slice(1); //expected b: [2,3]</td>
        <td>before ES5</td>
    </tr>
</table>